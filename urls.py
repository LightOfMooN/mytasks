from django.conf.urls import url
from django.contrib import admin
from django.urls import path

from mytasks.api.main import add_task, task_info

urlpatterns = [
    path('admin/', admin.site.urls),
    url(r'^api/v1/add_task/?$', add_task),
    url(r'^api/v1/task_info/?$', task_info),
]
