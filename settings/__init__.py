from settings.base import *


try:
    from settings.local import *
except ImportError as e:
    raise ImportError(
        'Файл "settings/local.py" не существует. '
        'Создайте его, при необходимости переопределив настройки из "settings/base.py"'
    )
