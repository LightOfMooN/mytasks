## Сервис выполнения задач

- Веб-сервер для постановки задач и получения информации (результатов выполнения) по поставленным задачам
- Сервис, выполняющий поставленные задачи

### Установка

Развернуть приложение

Установить зависимости
```
pip install -r requirements.txt
```

Создать файл локальных настроек
```
cd settings
cp local.py.default local.py
```

Внести нужные изменения (задать базу данных, настроить логирование и т.п.)

Произвести миграции базы данных
```
python manage.py migrate
```

### Запуск

Запуск web-сервера
```
python manage.py runserver
```

Запуск обработчика задач
```
python manage.py task_runner
```

Запуск обработчика задач в режиме демона
```
python manage.py task_runner start
```

### Примеры запросов

Постановка задачи
```
curl -X 'POST' -H 'Content-Type: application/json' -d '{"task_name": "multiprint", "params": {"msg": "hello world", "count": 5}}' 'http://localhost:8000/api/v1/add_task'
```

Получение информации по задаче (проверка статуса, получение результата)
```
curl -X 'POST' -H 'Content-Type: application/json' -d '{"task_id": 1}' 'http://localhost:8000/api/v1/task_info'
```
