import os
import errno


class PIDLockFile(object):

    def __init__(self, path):
        self.unique_name = path
        self.path = path

    def __enter__(self):
        self.acquire()
        return self

    def __exit__(self, *_exc):
        self.release()

    def read_pid(self):
        pid = None
        try:
            pidfile = open(self.path, 'r')
        except IOError:
            pass
        else:
            line = pidfile.readline().strip()
            try:
                pid = int(line)
            except ValueError:
                pass
            pidfile.close()

        return pid

    def is_locked(self):
        return os.path.exists(self.path)

    def i_am_locking(self):
        return self.is_locked() and os.getpid() == self.read_pid()

    def acquire(self):
        if self.is_locked():
            if self.i_am_locking():
                raise Exception(f'{self.path} is already locked')
            elif self.is_pidfile_alive():
                raise Exception(f'{self.path} daemon run with PID {self.read_pid()}')

        try:
            self.write_pid_to_pidfile()
        except OSError as exc:
            if exc.errno == errno.EEXIST:
                raise Exception(f'{self.path} is already locked')
            else:
                raise Exception(f'failed to create {self.path}')
        else:
            return

    def is_pidfile_alive(self):
        pidfile_pid = self.read_pid()

        if pidfile_pid:
            try:
                os.kill(pidfile_pid, 0)  # TODO: killpg
            except ProcessLookupError:
                result = False
            else:
                result = True
        else:
            result = False
        return result

    def release(self):
        if not self.is_locked():
            raise Exception(f'{self.path} is not locked')
        if not self.i_am_locking():
            raise Exception(f'{self.path} is locked, but not by me')
        self.remove_existing_pidfile()

    def break_lock(self):
        self.remove_existing_pidfile()

    def remove_existing_pidfile(self):
        try:
            os.remove(self.path)
        except OSError as exc:
            if exc.errno == errno.ENOENT:
                pass
            else:
                raise

    def write_pid_to_pidfile(self):
        open_flags = (os.O_CREAT | os.O_WRONLY)
        open_mode = 0o644
        pidfile_fd = os.open(self.path, open_flags, open_mode)
        pidfile = os.fdopen(pidfile_fd, 'w')
        pid = os.getpid()
        pidfile.write(f'{pid}\n')
        pidfile.close()
