import atexit
import os
import signal
import sys

from mytasks.daemon.pid_file import PIDLockFile


class DaemonContext:
    def __init__(self, pidfile=None, stdin=None, stdout=None, stderr=None):
        self.pidfile = pidfile
        self.stdin = stdin
        self.stdout = stdout
        self.stderr = stderr
        self._is_open = False

    @property
    def is_open(self):
        return self._is_open

    def open(self):
        if self.is_open:
            return

        self.detach_process_context()

        if self.stdout:
            self.redirect_stream(sys.stdout, self.stdout)
        if self.stderr:
            self.redirect_stream(sys.stderr, self.stderr)

        if self.pidfile is not None:
            self.pidfile.__enter__()

        self._is_open = True

        atexit.register(self.close)

    def __enter__(self):
        self.open()
        return self

    def close(self):
        if not self.is_open:
            return

        if self.pidfile is not None:
            self.pidfile.__exit__(None, None, None)

        self._is_open = False

    def __exit__(self, exc_type, exc_value, traceback):
        self.close()

    @staticmethod
    def detach_process_context():
        def fork_then_exit_parent(error_message):
            try:
                pid = os.fork()
                if pid > 0:
                    os._exit(0)
            except OSError as exc:
                error = Exception(f'{error_message}: [{exc.errno:d}] {exc.strerror}')
                raise error

        fork_then_exit_parent(error_message='Failed first fork')
        os.setsid()
        fork_then_exit_parent(error_message='Failed second fork')

    @staticmethod
    def redirect_stream(system_stream, target_stream):
        if target_stream is None:
            target_fd = os.open(os.devnull, os.O_RDWR)
        else:
            target_fd = target_stream.fileno()
        os.dup2(target_fd, system_stream.fileno())


class DaemonRunner(object):
    def __init__(self, app, pidfile_path, stdout_path=None, stderr_path=None):
        self.app = app
        self.daemon_context = DaemonContext()

        if stdout_path is not None:
            self.daemon_context.stdout = open(stdout_path, 'w+')
        if stderr_path is not None:
            self.daemon_context.stderr = open(stderr_path, 'w+')

        self.pidfile = PIDLockFile(pidfile_path)
        self.daemon_context.pidfile = self.pidfile

    def _start(self):
        with self.daemon_context:
            self.app()

    def _terminate_daemon_process(self):
        pid = self.pidfile.read_pid()
        try:
            os.killpg(
                os.getpgid(pid),
                signal.SIGINT
            )
        except OSError as exc:
            raise Exception(f'Failed to terminate {pid:d}: {exc}')

    def _stop(self):
        if not self.pidfile.is_locked():
            raise Exception(f'PID file {self.pidfile.path!r} not locked')

        self._terminate_daemon_process()

    action_funcs = {
        'start': _start,
        'stop': _stop,
    }

    def do_action(self, action):
        if action not in self.action_funcs:
            raise Exception

        self.action_funcs[action](self)
