from django.contrib.postgres.fields import JSONField
from django.db import models, transaction

from mytasks.tasks import get_task_by_name


class BaseModel(models.Model):
    class Meta:
        abstract = True

    created_at = models.DateTimeField(
        auto_now_add=True,
        verbose_name='Дата создания',
    )
    updated_at = models.DateTimeField(
        auto_now=True,
        verbose_name='Дата обновления',
    )


class Task(BaseModel):
    """
    Модель хранения данных задачи
    """
    class Meta:
        verbose_name = 'Задача'
        verbose_name_plural = 'Задачи'
        index_together = (
            ('status', 'created_at'),
        )

    STATUS_NEW = 'new'
    STATUS_IN_PROGRESS = 'in_progress'
    STATUS_DONE = 'done'
    STATUS_ERROR = 'error'

    STATUS_CHOICES = (
        (STATUS_NEW, 'Новая'),
        (STATUS_IN_PROGRESS, 'В работе'),
        (STATUS_DONE, 'Готово'),
        (STATUS_ERROR, 'Ошибка'),
    )

    status = models.CharField(
        max_length=32,
        choices=STATUS_CHOICES,
        default=STATUS_NEW,
        verbose_name='Статус',
        db_index=True,
    )
    name = models.CharField(
        max_length=512,
        verbose_name='Кодовое наименование задачи',
    )
    params = JSONField(
        blank=True,
        default=dict,
        verbose_name='Параметры задачи',
    )
    result = JSONField(
        blank=True,
        default=dict,
        verbose_name='Результат выполнения',
    )
    error_message = models.TextField(
        blank=True,
        default='',
        verbose_name='Текст ошибки',
    )

    def __str__(self):
        return f'Задача с id="{self.pk}", name="{self.name}", params="{self.params}"'

    @classmethod
    @transaction.atomic
    def fetch_task(cls):
        """
        Получить задачу для выполнения

        Returns:
            Task|None: задача, либо None, если задача отсутствует
        """
        task = cls.objects.filter(status=cls.STATUS_NEW).select_for_update().first()
        if not task:
            return
        task.status = cls.STATUS_IN_PROGRESS
        task.save()
        return task

    @classmethod
    def get_task(cls, task_id):
        return cls.objects.filter(pk=task_id).first()

    @classmethod
    def add_task(cls, name, params):
        """
        Добавить задачу

        Args:
            name (str): название задачи
            params (dict): параметры задачи

        Returns:
            Task: объект задачи
        """
        return cls.objects.create(
            status=cls.STATUS_NEW,
            name=name,
            params=params or {},
        )

    def execute(self):
        """
        Обработать задачу

        Returns:
            Task: объект задачи
        """
        handler = get_task_by_name(self.name)
        try:
            result = handler.run(**self.params)
            self.status = self.STATUS_DONE
            self.result = result
        except Exception as e:
            self.status = self.STATUS_ERROR
            self.error_message = str(e)
        self.save()
        return self
