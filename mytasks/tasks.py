import logging

import jsonschema

from mytasks.errors import ValidationError


logger = logging.getLogger(__name__)


handlers = {}


class BaseTask:
    """
    Базовый класс задачи
    """
    name = None
    json_scheme = None

    def __init__(self, name=None, json_schema=None, fn=None):
        if name:
            self.name = name
        if json_schema:
            self.json_schema = json_schema
        if fn:
            self.run = fn

    def run(self, **__):
        """
        Метод, выполняющийся при обработке задачи.
        """
        raise NotImplementedError()

    def validate(self, params):
        """
        Валидация параметров на соответствие схеме

        Args:
            params: объект параметро для валидации

        Raises:
            ValidationError: в случае неуспешной валидации
        """
        if not self.json_schema:
            return
        try:
            jsonschema.validate(
                params,
                self.json_schema,
            )
            return
        except jsonschema.exceptions.ValidationError:
            raise ValidationError('Параметры задачи не соответствуют схеме')

    def execute(self, params):
        """
        Выполнить задачу
        """
        self.validate(params)
        return self.run(**params)


def task(name, json_schema=None):
    """
    Декоратор, регистрирующий задачу по названию

    Args:
        name (str): название задачи
        json_schema (dict): схема параметров задачи
    """
    def wrapped(fn):
        if name in handlers:
            raise AttributeError(f'Уже присутствует обработчик задачи с name="{name}"')
        handlers[name] = BaseTask(
            name=name,
            json_schema=json_schema,
            fn=fn,
        )
    return wrapped


def register_tasks():
    """
    Зарегистрировать задачи на базе класса BaseTask
    """
    for c in BaseTask.__subclasses__():
        handlers[c.name] = c()


def get_task_by_name(name):
    """
    Получить задачу по её названию

    Args:
        name (str): название задачи
    """
    return handlers[name]


register_tasks()
