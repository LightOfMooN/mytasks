import datetime
import logging
import os
import signal
import time
import multiprocessing as mp

from mytasks.models import Task
from mytasks.tasks import register_tasks


logger = logging.getLogger(__name__)


class TaskProcess:
    def __init__(self, fetch_lock, number=None):
        self.stopping = False
        self.number = number
        self.pid = os.getpid()
        self.fetch_lock = fetch_lock

    @property
    def is_stopping(self):
        return self.stopping

    @property
    def log_prefix(self):
        return f'Процесс-{self.number or self.pid} {datetime.datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S")}'

    def log_message(self, message='', level='info'):
        message = f'{self.log_prefix}: {message}'
        {
            'info': logger.info,
            'error': logger.error,
        }[level](message)

    def stop(self):
        self.log_message('Остановка')
        self.stopping = True

    def run(self):
        self.log_message('Запущен')
        fetch_delay = False
        while True:
            try:
                with self.fetch_lock:
                    if self.is_stopping:
                        break
                    if fetch_delay:
                        time.sleep(5)
                    t = Task.fetch_task()
                if not t:
                    # Если очередь пустая, установим задержку на следующую попытку получения задачи
                    fetch_delay = True
                    continue
                fetch_delay = False
                self.log_message(f'Получена задача: {t}')
                t.execute()
                if t.status == Task.STATUS_DONE:
                    self.log_message(f'Задача с id="{t.pk}" выполнена успешно')
                elif t.status == Task.STATUS_ERROR:
                    self.log_message(
                        f'При выполнении задачи с id="{t.pk}" возникла ошибка: "{t.error_message}"',
                        level='error',
                    )
            except InterruptedError:  # для Windows
                pass
        self.log_message('Остановлен')


def run_task_process(fetch_lock, number=None):
    task_process = TaskProcess(
        fetch_lock,
        number,
    )
    signal.signal(
        signal.SIGINT,
        lambda *_: task_process.stop()
    )
    task_process.run()


class TaskExecutor:
    def __init__(self):
        self.processes = []
        self.pid = os.getpid()
        self.fetch_lock = mp.Lock()

    def run(self, proc_number=None):
        if not proc_number:
            proc_number = mp.cpu_count()

        for i in range(proc_number):
            self.processes.append(
                mp.Process(
                    target=run_task_process,
                    args=(
                        self.fetch_lock,
                    ),
                )
            )
        for p in self.processes:
            p.start()
        logger.info(f'Обработчик задач запущен. PID: {self.pid}')
        signal.signal(signal.SIGINT, lambda *_: None)
        for p in self.processes:
            p.join()
        logger.info(f'Обработчик задач остановлен. PID: {self.pid}')


def run():
    """
    Запуск обработчика задач
    """
    register_tasks()
    t = TaskExecutor()
    t.run()
