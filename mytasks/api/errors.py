

class APIError(Exception):
    code = 500
    msg = 'Внутренняя ошибка сервера'

    def __init__(self, code=None, msg=None):
        if code is not None:
            self.code = code
        if msg is not None:
            self.msg = msg

    def __str__(self):
        return f'{self.msg}'


class ValidationError(APIError):
    code = 10
    msg = 'Ошибка валидации данных'


class ParameterRequired(ValidationError):
    code = 11
    msg = 'Отсутствует обязательный параметр'


class ExecutionError(APIError):
    code = 100
    msg = 'Ошибка выполнения'


class NotFoundError(APIError):
    code = 404
    msg = 'Объект не найден'
