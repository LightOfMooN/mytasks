import json

from django.http import HttpResponse

from mytasks import errors as task_errors
from mytasks import tasks
from mytasks.api import errors as api_errors
from mytasks.models import Task


def api_method(fn):
    def wrapped(request):
        response = {
            'status': 'OK'
        }
        try:
            data = json.loads(
                request.body
            )
            response.update(
                fn(data)
            )
        except api_errors.APIError as e:
            response.update({
                'status': 'ERROR',
                'error_code': e.code,
                'error_msg': str(e),
            })
        except Exception as e:
            response.update({
                'status': 'ERROR',
                'error_code': api_errors.APIError.code,
                'error_msg': str(e),
            })
        return HttpResponse(
            json.dumps(response)
        )
    return wrapped


@api_method
def add_task(data):
    if 'task_name' not in data:
        raise api_errors.ParameterRequired(
            msg='Не задан обязательный параметр "task_name"'
        )
    task_name = data['task_name']
    task_params = data.get('params', {})
    try:
        task = tasks.get_task_by_name(task_name)
    except KeyError:
        raise api_errors.NotFoundError(
            msg=f'Не найден обработчик задачи с name="{task_name}"'
        )
    try:
        task.validate(task_params)
        return {
            'task_id': Task.add_task(
                name=task_name,
                params=task_params,
            ).pk
        }
    except task_errors.ValidationError:
        raise api_errors.ValidationError(
            msg='Параметры задачи не соответствуют схеме'
        )


@api_method
def task_info(data):
    task_id = data['task_id']
    task = Task.get_task(task_id=task_id)
    if not task:
        raise api_errors.NotFoundError(
            msg='Задача не найдена',
        )
    if task.status == Task.STATUS_ERROR:
        raise api_errors.ExecutionError(
            msg=task.error_message,
        )
    return {
        'id': task.id,
        'status': task.status.upper(),
        'result': task.result,
    }
