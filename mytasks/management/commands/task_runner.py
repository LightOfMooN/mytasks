import logging
import os

from django.conf import settings
from django.core.management import BaseCommand

from mytasks import task_runner
from mytasks.daemon import DaemonRunner

logger = logging.getLogger(__name__)


def runner():
    task_runner.run()


class Command(BaseCommand):
    help = 'Запускает обработчик задач'

    def add_arguments(self, parser):
        parser.add_argument(
            'command', metavar='command', nargs='?', default='default')

    def handle(self, command, **options):
        if command == 'default':
            runner()
            return

        DaemonRunner(
            app=runner,
            pidfile_path=os.path.join(settings.BASE_DIR, 'mytasks.pid'),
        ).do_action(command)
