from functools import reduce

from mytasks import tasks


@tasks.task(
    name='multiprint',
    json_schema={
        'type': 'object',
        'properties': {
            'msg': {'type': 'string'},
            'count': {'type': 'integer', 'minimum': 1}
        },
        'required': ['msg']
    }
)
def multi_print(msg, count=1):
    return '\n'.join(msg for _ in range(count))


class Multiply(tasks.BaseTask):
    name = 'mult'
    json_schema = {
        'type': 'object',
        'properties': {
            'operands': {
                'type': 'array',
                'minItems': '1',
                'items': {
                    'type': 'number'
                }
            }
        },
        'required': ['operands'],
    }

    @staticmethod
    def run(operands):
        return reduce(lambda x, y: x*y, operands)
